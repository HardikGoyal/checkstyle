package org.example;

import java.util.ArrayList;
import java.util.List;

public class SpotBugsExample {
    public static void main(String[] args) {
        // Press Opt+Enter with your caret at the highlighted text to see how
        // IntelliJ IDEA suggests fixing it.
        System.out.printf("Hello and welcome!");

        // Press Ctrl+R or click the green arrow button in the gutter to run the code.
        for (int i = 1; i <= 5; i++) {

            // Press Ctrl+D to start debugging your code. We have set one breakpoint
            // for you, but you can always add more by pressing Cmd+F8.
            System.out.println("i = " + i);
        }

        ArrayList<Integer> array=null;
        array.add(8);
        int i=1;

        if(i==0){
            array=new ArrayList<>();
        }

        array.add(8);
    }
}
//
//src/main/java/org/example/Animal.java
//target/classes/org/example/Animal

//src/main/java/org/example/AnimalTests.java
