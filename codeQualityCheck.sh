#!/bin/sh

diffList=''
diffCompiledClasses=''

readDiffFile () {
  diffFile=target/diffFiles.txt

  while read -r file; do
      
    if [[ $file =~ src/main/java/.*java ]]; then
      
      fileName=${file#src/main/java/}

      diffList+=$fileName
      diffList+=,

      fileNameWithoutExtension=${fileName%.java}
      
      diffCompiledClasses+=$fileNameWithoutExtension
      diffCompiledClasses+=,
    fi

    if [[ $file =~ src/test/java/.*java ]]; then

      fileName=${file#src/test/java/}

      diffList+=$fileName
      diffList+=,

      fileNameWithoutExtension=${fileName%.java}
      
      diffCompiledClasses+=$fileNameWithoutExtension
      diffCompiledClasses+=,
    fi
  done <$diffFile
        
}
      
readDiffFile

mvn checkstyle:checkstyle -Dcheckstyle.includes=$diffList
mvn spotbugs:spotbugs -Dspotbugs.onlyAnalyze=$diffCompiledClasses
